package com.nguyendoha.fresher_fpt_information_system.repository;

import com.nguyendoha.fresher_fpt_information_system.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    public Optional<Customer> findByUserName(String username);

    public Boolean existsByUserName(String username);

    public Boolean existsByEmail(String email);
}
