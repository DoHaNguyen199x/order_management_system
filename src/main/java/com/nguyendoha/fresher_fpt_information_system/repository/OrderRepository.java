package com.nguyendoha.fresher_fpt_information_system.repository;

import com.nguyendoha.fresher_fpt_information_system.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
