package com.nguyendoha.fresher_fpt_information_system.repository;

import com.nguyendoha.fresher_fpt_information_system.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
}
