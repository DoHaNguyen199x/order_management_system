package com.nguyendoha.fresher_fpt_information_system.repository;

import com.nguyendoha.fresher_fpt_information_system.entity.Role;
import com.nguyendoha.fresher_fpt_information_system.entity.enums.ERole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    public Optional<Role> findByName(ERole name);
}
