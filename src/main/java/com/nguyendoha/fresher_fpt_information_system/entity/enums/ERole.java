package com.nguyendoha.fresher_fpt_information_system.entity.enums;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
