package com.nguyendoha.fresher_fpt_information_system.entity.enums;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public enum OrderStatus {
    CREATED,
    PAID,
    CANCELLED
}
