package com.nguyendoha.fresher_fpt_information_system.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Entity
@Data
@Table(name = "Tbl_product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull(message = "product not null !!!")
    @Size(min = 10, max = 100, message = "name product > 10 and < 100 character !!!")
    private String name;
    @NotNull(message = "price not null !!!")
    private Double price;
    @NotNull(message = "available not null !!!")
    private Long available;
    @JsonIgnore
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;
}
