package com.nguyendoha.fresher_fpt_information_system.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class PasswordError extends RuntimeException {
    public PasswordError(String message) {
        super(message);
    }
}
