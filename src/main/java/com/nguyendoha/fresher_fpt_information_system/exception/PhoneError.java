package com.nguyendoha.fresher_fpt_information_system.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class PhoneError extends RuntimeException {
    public PhoneError(String message) {
        super(message);
    }
}
