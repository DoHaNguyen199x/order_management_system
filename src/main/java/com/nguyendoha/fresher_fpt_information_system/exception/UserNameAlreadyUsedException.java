package com.nguyendoha.fresher_fpt_information_system.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class UserNameAlreadyUsedException extends RuntimeException {
    public UserNameAlreadyUsedException(String message) {
        super(message);
    }
}
