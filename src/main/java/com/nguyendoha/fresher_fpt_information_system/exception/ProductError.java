package com.nguyendoha.fresher_fpt_information_system.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class ProductError extends RuntimeException {
    public ProductError(String message) {
        super(message);
    }
}
