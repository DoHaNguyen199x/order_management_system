package com.nguyendoha.fresher_fpt_information_system.exception;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class OrderError extends RuntimeException{
    public OrderError(String message) {
        super(message);
    }
}
