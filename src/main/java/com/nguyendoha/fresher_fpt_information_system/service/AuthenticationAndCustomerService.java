package com.nguyendoha.fresher_fpt_information_system.service;

import com.nguyendoha.fresher_fpt_information_system.dto.request.LoginRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.request.SignupRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.response.CustomerResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public interface AuthenticationAndCustomerService {
    public ResponseEntity<?> authenticateUser(LoginRequest loginRequest);

    public ResponseEntity<?> registerUser(SignupRequest signUpRequest);

    public ResponseEntity<?> logoutUser();

    public Page<CustomerResponse> findAllUser(Pageable pageable);

    public CustomerResponse findById(Long id);

    public CustomerResponse updateUser(Long id, SignupRequest signupRequest);

    public CustomerResponse removeRoleUser(Long id, SignupRequest signupRequest);

    public CustomerResponse addRoleUser(Long id, SignupRequest signupRequest);

    public ResponseEntity<?> deleteUser(Long id);
}
