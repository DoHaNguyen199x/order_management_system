package com.nguyendoha.fresher_fpt_information_system.service;

import com.nguyendoha.fresher_fpt_information_system.dto.request.OrderRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.response.OrderResponse;
import com.nguyendoha.fresher_fpt_information_system.entity.Order;

public interface OrderService extends MethodBasic<OrderRequest, OrderResponse, Long> {
    public OrderResponse addOrderItem(Long id, OrderRequest orderRequest);

    public OrderResponse removeOrderItem(Long id, OrderResponse orderResponse);

    public OrderResponse updateStatusPaid(Long id);
    public OrderResponse updateStatusCancel(Long id);
}
