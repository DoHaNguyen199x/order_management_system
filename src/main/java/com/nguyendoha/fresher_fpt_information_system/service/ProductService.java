package com.nguyendoha.fresher_fpt_information_system.service;

import com.nguyendoha.fresher_fpt_information_system.entity.Product;

public interface ProductService extends MethodBasic<Product, Product, Long> {
}
