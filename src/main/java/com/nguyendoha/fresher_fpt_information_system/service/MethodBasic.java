package com.nguyendoha.fresher_fpt_information_system.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface MethodBasic<Request, Response, TypeId> {
    public Page<Response> findAll(Pageable pageable);

    public Response findById(TypeId id);

    public Response save(Request request);

    public Response update(TypeId id, Request request);

    public ResponseEntity<?> Delete(TypeId id);
}
