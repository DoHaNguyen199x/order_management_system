package com.nguyendoha.fresher_fpt_information_system.service.imp;

import com.nguyendoha.fresher_fpt_information_system.constant.Constant;
import com.nguyendoha.fresher_fpt_information_system.controller.ExceptionHandler.Message;
import com.nguyendoha.fresher_fpt_information_system.entity.Customer;
import com.nguyendoha.fresher_fpt_information_system.entity.Role;
import com.nguyendoha.fresher_fpt_information_system.entity.enums.ERole;
import com.nguyendoha.fresher_fpt_information_system.exception.CustomerNotFound;
import com.nguyendoha.fresher_fpt_information_system.exception.RoleNotFoundException;
import com.nguyendoha.fresher_fpt_information_system.util.CheckSignupRequest;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import com.nguyendoha.fresher_fpt_information_system.config.JwtUtils;
import com.nguyendoha.fresher_fpt_information_system.dto.request.LoginRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.request.SignupRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.response.CustomerResponse;
import com.nguyendoha.fresher_fpt_information_system.repository.CustomerRepository;
import com.nguyendoha.fresher_fpt_information_system.repository.RoleRepository;
import com.nguyendoha.fresher_fpt_information_system.service.AuthenticationAndCustomerService;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Service
public class AuthenticationAndCustomerServiceImp implements AuthenticationAndCustomerService {
    private final AuthenticationManager authenticationManager;
    private final CustomerRepository customerRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder encoder;
    private final JwtUtils jwtUtils;
    private final CheckSignupRequest checkSignupRequest;

    @Autowired
    public AuthenticationAndCustomerServiceImp(AuthenticationManager authenticationManager,
                                               CustomerRepository customerRepository, RoleRepository roleRepository,
                                               PasswordEncoder encoder, JwtUtils jwtUtils,
                                               CheckSignupRequest checkSignupRequest) {
        this.authenticationManager = authenticationManager;
        this.customerRepository = customerRepository;
        this.roleRepository = roleRepository;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
        this.checkSignupRequest = checkSignupRequest;
    }

    @Transactional
    @Override
    public ResponseEntity<?> authenticateUser(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUserName(),
                        loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
                .body(CustomerResponse.builder()
                        .id(userDetails.getId())
                        .name(userDetails.getName())
                        .email(userDetails.getEmail())
                        .phone(userDetails.getPhone())
                        .address(userDetails.getAddress())
                        .roles(roles)
                        .build());
    }

    @Override
    public ResponseEntity<?> registerUser(SignupRequest signUpRequest) {
        checkSignupRequest.check(signUpRequest);
        Customer customer = Customer.builder()
                .userName(signUpRequest.getUserName())
                .email(signUpRequest.getEmail())
                .password(encoder.encode(signUpRequest.getPassword()))
                .passwordAgain(encoder.encode(signUpRequest.getPassword()))
                .phone(signUpRequest.getPhone())
                .orders(Collections.emptyList())
                .name(signUpRequest.getName())
                .address(signUpRequest.getAddress())
                .build();
        List<Role> roles = new ArrayList<>();
        signUpRequest.getRoles().forEach(s -> {
            Role role = roleRepository.findByName(ERole.valueOf(s)).orElseThrow(() -> {
                throw new RoleNotFoundException("Role does not exist !!!");
            });
            roles.add(role);
        });
        customer.setRoles(roles);
        customerRepository.save(customer);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Message.builder()
                        .code(Constant.SUCCESSFUL)
                        .time(LocalDateTime.now())
                        .message("successful registration !!!")
                        .build());
    }

    @Override
    public ResponseEntity<?> logoutUser() {
        ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
                .body(Message.builder()
                        .code(Constant.LOG_OUT)
                        .time(LocalDateTime.now())
                        .message("You've been signed out!")
                        .build());
    }

    @Override
    public Page<CustomerResponse> findAllUser(Pageable pageable) {

        return customerRepository.findAll(pageable).map(CustomerResponse::fromEntity);
    }

    @Override
    public CustomerResponse findById(Long id) {
        return customerRepository.findById(id).map(CustomerResponse::fromEntity).orElseThrow(() -> {
            throw new CustomerNotFound("customer does not exist !!!");
        });
    }

    @Override
    public CustomerResponse updateUser(Long id, SignupRequest signupRequest) {
        checkSignupRequest.check(signupRequest);
        Customer customer = customerRepository.findById(id).orElseThrow(() -> {
            throw new CustomerNotFound("customer does not exist !!!");
        });
        customer.setUserName(signupRequest.getUserName());
        customer.setAddress(signupRequest.getAddress());
        customer.setName(customer.getName());
        customer.setEmail(customer.getEmail());
        customer.setPhone(customer.getPhone());
        customer.setPassword(encoder.encode(signupRequest.getPassword()));
        customer.setPasswordAgain(encoder.encode(signupRequest.getPassword()));
        return CustomerResponse.fromEntity(customerRepository.save(customer));
    }

    @Override
    public CustomerResponse removeRoleUser(Long id, SignupRequest signupRequest) {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> {
            throw new CustomerNotFound("customer does not exist !!!");
        });
        signupRequest.getRoles().forEach(s -> {
            Role role = roleRepository.findByName(ERole.valueOf(s)).orElseThrow(() -> {
                throw new RoleNotFoundException("role does not exist !!!");
            });
            customer.getRoles().remove(role);
        });
        return CustomerResponse.fromEntity(customerRepository.save(customer));
    }

    @Override
    public CustomerResponse addRoleUser(Long id, SignupRequest signupRequest) {
        Customer customer = customerRepository.findById(id).orElseThrow(() -> {
            throw new CustomerNotFound("customer does not exist !!!");
        });
        signupRequest.getRoles().forEach(s -> {
            Role role = roleRepository.findByName(ERole.valueOf(s)).orElseThrow(() -> {
                throw new RoleNotFoundException("role does not exist !!!");
            });
            customer.getRoles().add(role);
        });
        return CustomerResponse.fromEntity(customerRepository.save(customer));
    }

    @Override
    public ResponseEntity<?> deleteUser(Long id) {
        customerRepository.findById(id).orElseThrow(() -> {
            throw new CustomerNotFound("customer does not exist !!!");
        });
        customerRepository.deleteById(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Message.builder()
                        .code(Constant.SUCCESSFUL)
                        .time(LocalDateTime.now())
                        .message("Delete Customer successful !!!")
                        .build());
    }
}
