package com.nguyendoha.fresher_fpt_information_system.service.imp;

import com.nguyendoha.fresher_fpt_information_system.constant.Constant;
import com.nguyendoha.fresher_fpt_information_system.controller.ExceptionHandler.Message;
import com.nguyendoha.fresher_fpt_information_system.dto.request.OrderRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.response.OrderResponse;
import com.nguyendoha.fresher_fpt_information_system.entity.Customer;
import com.nguyendoha.fresher_fpt_information_system.entity.Order;
import com.nguyendoha.fresher_fpt_information_system.entity.OrderItem;
import com.nguyendoha.fresher_fpt_information_system.entity.Product;
import com.nguyendoha.fresher_fpt_information_system.entity.enums.OrderStatus;
import com.nguyendoha.fresher_fpt_information_system.exception.*;
import com.nguyendoha.fresher_fpt_information_system.repository.CustomerRepository;
import com.nguyendoha.fresher_fpt_information_system.repository.OrderItemRepository;
import com.nguyendoha.fresher_fpt_information_system.repository.OrderRepository;
import com.nguyendoha.fresher_fpt_information_system.repository.ProductRepository;
import com.nguyendoha.fresher_fpt_information_system.service.OrderService;
import com.nguyendoha.fresher_fpt_information_system.util.CheckOrderItemByOrder;
import com.nguyendoha.fresher_fpt_information_system.util.CheckOrderStatus;
import com.nguyendoha.fresher_fpt_information_system.util.CheckProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Service
public class OrderServiceImp implements OrderService {
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final OrderItemRepository orderItemRepository;
    private final CheckProduct checkProduct;
    private final CheckOrderStatus checkOrderStatus;
    private final CheckOrderItemByOrder checkOrderItemByOrder;

    @Autowired
    public OrderServiceImp(OrderRepository orderRepository, CustomerRepository customerRepository,
                           ProductRepository productRepository, OrderItemRepository orderItemRepository,
                           CheckProduct checkProduct, CheckOrderStatus checkOrderStatus,
                           CheckOrderItemByOrder checkOrderItemByOrder) {
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.orderItemRepository = orderItemRepository;
        this.checkProduct = checkProduct;
        this.checkOrderStatus = checkOrderStatus;
        this.checkOrderItemByOrder = checkOrderItemByOrder;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<OrderResponse> findAll(Pageable pageable) {
        return orderRepository.findAll(pageable).map(OrderResponse::fromEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public OrderResponse findById(Long aLong) {
        return orderRepository.findById(aLong).map(OrderResponse::fromEntity).orElseThrow(() -> {
            throw new OrderNotFound("order does not exist !!!");
        });
    }

    @Transactional
    @Override
    public OrderResponse save(OrderRequest orderRequest) {
        Customer customer = customerRepository.findById(orderRequest.getCustomer()).orElseThrow(() -> {
            throw new CustomerNotFound("customer does not exist !!!");
        });
        Order order = Order.builder()
                .orderDateTime(LocalDateTime.now())
                .status(OrderStatus.CREATED)
                .orderItems(Collections.emptyList())
                .totalAmount(0d)
                .customer(customer)
                .build();
        Order setOrderItem = orderRepository.save(order);
        if (orderRequest.getOrderItems() != null) {
            List<OrderItem> orderItems = new ArrayList<>();
            orderRequest.getOrderItems().forEach(orderItemRequest -> {
                Product product = productRepository.findById(orderItemRequest.getProduct()).orElseThrow(() -> {
                    throw new ProductNotFound("product does not exist !!!");
                });
                checkProduct.check(product, orderItemRequest);
                OrderItem orderItem = OrderItem.builder()
                        .product(product)
                        .order(setOrderItem)
                        .amount(orderItemRequest.getQuantity() * product.getPrice())
                        .quantity(orderItemRequest.getQuantity())
                        .build();
                product.setAvailable(product.getAvailable() - orderItemRequest.getQuantity());
                productRepository.save(product);
                setOrderItem.setTotalAmount(setOrderItem.getTotalAmount() + orderItem.getAmount());
                orderItems.add(orderItem);
            });
            setOrderItem.setOrderItems(orderItems);
            return OrderResponse.fromEntity(orderRepository.save(setOrderItem));
        } else {
            return OrderResponse.fromEntity(setOrderItem);
        }
    }

    @Transactional
    @Override
    public OrderResponse update(Long aLong, OrderRequest orderRequest) {
        Order update = orderRepository.findById(aLong).orElseThrow(() -> {
            throw new OrderNotFound("order does not exist !!!");
        });
        // only update status
        update.setStatus(OrderStatus.valueOf(orderRequest.getStatus()));
        return OrderResponse.fromEntity(orderRepository.save(update));
    }

    @Transactional
    @Override
    public ResponseEntity<?> Delete(Long aLong) {
        orderRepository.findById(aLong).orElseThrow(() -> {
            throw new OrderNotFound("order does not exist !!!");
        });
        orderRepository.deleteById(aLong);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Message.builder()
                        .code(Constant.SUCCESSFUL)
                        .time(LocalDateTime.now())
                        .message("Delete Order successful !!!")
                        .build());
    }

    @Transactional
    @Override
    public OrderResponse addOrderItem(Long id, OrderRequest orderRequest) {
        Order order = orderRepository.findById(id).orElseThrow(() -> {
            throw new OrderNotFound("order does not exist !!!");
        });
        checkOrderStatus.check(order);
        List<OrderItem> orderItems = new ArrayList<>();
        orderRequest.getOrderItems().forEach(orderItemRequest -> {
            Product product = productRepository.findById(orderItemRequest.getProduct()).orElseThrow(() -> {
                throw new ProductNotFound("product does not exist !!!");
            });
            checkProduct.check(product, orderItemRequest);
            OrderItem orderItem = OrderItem.builder()
                    .product(product)
                    .order(order)
                    .amount(orderItemRequest.getQuantity() * product.getPrice())
                    .quantity(orderItemRequest.getQuantity())
                    .build();
            product.setAvailable(product.getAvailable() - orderItemRequest.getQuantity());
            productRepository.save(product);
            order.setTotalAmount(order.getTotalAmount() + orderItem.getAmount());
            orderItems.add(orderItem);
        });
        order.setOrderItems(orderItems);
        return OrderResponse.fromEntity(orderRepository.save(order));
    }

    @Override
    public OrderResponse removeOrderItem(Long id, OrderResponse orderResponse) {
        Order order = orderRepository.findById(id).orElseThrow(() -> {
            throw new OrderNotFound("order does not exist !!!");
        });
        checkOrderStatus.check(order);
        orderResponse.getOrderItems().forEach(orderItemResponse -> {
            OrderItem orderItem = orderItemRepository.findById(orderItemResponse.getId()).orElseThrow(() -> {
                throw new OrderItemNotFound("order item does not exist !!!");
            });
            checkOrderItemByOrder.check(order, orderItem);
            orderItemRepository.deleteById(orderItem.getId());
            Product product = orderItem.getProduct();
            product.setAvailable(product.getAvailable() + orderItem.getQuantity());
            productRepository.save(product);
            order.setTotalAmount(order.getTotalAmount() - orderItem.getAmount());
            if (order.getTotalAmount() < 0) {
                order.setTotalAmount(0d);
            }
        });
        return OrderResponse.fromEntity(orderRepository.save(order));
    }

    @Transactional
    @Override
    public OrderResponse updateStatusPaid(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> {
            throw new OrderNotFound("order does not exist !!!");
        });
        order.setStatus(OrderStatus.PAID);
        return OrderResponse.fromEntity(orderRepository.save(order));
    }

    @Transactional
    @Override
    public OrderResponse updateStatusCancel(Long id) {
        Order order = orderRepository.findById(id).orElseThrow(() -> {
            throw new OrderNotFound("order does not exist !!!");
        });
        order.setStatus(OrderStatus.CANCELLED);
        return OrderResponse.fromEntity(orderRepository.save(order));
    }
}
