package com.nguyendoha.fresher_fpt_information_system.service.imp;

import com.nguyendoha.fresher_fpt_information_system.constant.Constant;
import com.nguyendoha.fresher_fpt_information_system.controller.ExceptionHandler.Message;
import com.nguyendoha.fresher_fpt_information_system.entity.Product;
import com.nguyendoha.fresher_fpt_information_system.exception.ProductNotFound;
import com.nguyendoha.fresher_fpt_information_system.repository.ProductRepository;
import com.nguyendoha.fresher_fpt_information_system.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Product> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Product findById(Long aLong) {
        return productRepository.findById(aLong).orElseThrow(() -> {
            throw new ProductNotFound("product does not exist !!!");
        });
    }

    @Transactional
    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Transactional
    @Override
    public Product update(Long aLong, Product product) {
        Product update = productRepository.findById(aLong).orElseThrow(() -> {
            throw new ProductNotFound("product does not exist !!!");
        });
        update.setAvailable(product.getAvailable());
        update.setName(product.getName());
        update.setPrice(product.getPrice());
        return productRepository.save(update);
    }

    @Transactional
    @Override
    public ResponseEntity<?> Delete(Long aLong) {
        productRepository.findById(aLong).orElseThrow(() -> {
            throw new ProductNotFound("product does not exist !!!");
        });
        productRepository.deleteById(aLong);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(Message.builder()
                        .code(Constant.SUCCESSFUL)
                        .time(LocalDateTime.now())
                        .message("Delete Product successful !!!")
                        .build());
    }
}
