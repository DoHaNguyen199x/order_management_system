package com.nguyendoha.fresher_fpt_information_system.constant;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
public class Constant {
    public static final String SUCCESSFUL = "SUCCESSFUL";
    public static final String CUSTOMER_NOT_FOUND = "CUSTOMER_NOT_FOUND";
    public static final String ORDER_ITEM_NOT_FOUND = "ORDER_ITEM_NOT_FOUND";
    public static final String ORDER_ITEM_ERROR = "ORDER_ITEM_ERROR";
    public static final String PRODUCT_ERROR = "PRODUCT_ERROR";
    public static final String PRODUCT_NOT_FOUND = "PRODUCT_NOT_FOUND";
    public static final String ORDER_ERROR = "ORDER_ERROR";
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR";
    public static final String VALIDATION_ERROR = "VALIDATION_ERROR";
    public static final String LOG_OUT = "LOG_OUT";
}
