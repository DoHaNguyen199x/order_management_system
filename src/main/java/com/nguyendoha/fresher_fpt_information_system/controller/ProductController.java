package com.nguyendoha.fresher_fpt_information_system.controller;

import com.nguyendoha.fresher_fpt_information_system.entity.Product;
import com.nguyendoha.fresher_fpt_information_system.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@RestController
@RequestMapping("/api/product")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{n}/{s}")
    public Page<Product> findAll(@PathVariable(name = "n") Integer n,
                                 @PathVariable(name = "s") Integer s) {
        return productService.findAll(PageRequest.of(n, s));
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable(name = "id") Long aLong) {
        return productService.findById(aLong);
    }

    @PostMapping("/")
    public Product save(@Valid @RequestBody Product product) {
        return productService.save(product);
    }

    @PutMapping("/{id}")
    public Product update(@Valid @PathVariable(name = "id") Long aLong, @RequestBody Product product) {
        return productService.update(aLong, product);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> Delete(@PathVariable(name = "id") Long aLong) {
        return productService.Delete(aLong);
    }
}
