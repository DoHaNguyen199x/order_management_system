package com.nguyendoha.fresher_fpt_information_system.controller;

import com.nguyendoha.fresher_fpt_information_system.dto.request.OrderRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.response.OrderResponse;
import com.nguyendoha.fresher_fpt_information_system.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */

@RestController
@RequestMapping("/api/order")
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/{n}/{s}")
    public Page<OrderResponse> findAll(@PathVariable(name = "n") Integer n,
                                       @PathVariable(name = "s") Integer s) {
        return orderService.findAll(PageRequest.of(n, s));
    }

    @GetMapping("/{id}")
    public OrderResponse findById(@PathVariable(name = "id") Long aLong) {
        return orderService.findById(aLong);
    }

    @PostMapping("/")
    public OrderResponse save(@Valid @RequestBody OrderRequest orderRequest) {
        return orderService.save(orderRequest);
    }


    @PutMapping("/{id}")
    public OrderResponse update(@Valid @PathVariable(name = "id") Long aLong,
                                @RequestBody OrderRequest orderRequest) {
        return orderService.update(aLong, orderRequest);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> Delete(@PathVariable(name = "id") Long aLong) {
        return orderService.Delete(aLong);
    }

    @PostMapping("/addorderitem/{id}")
    public OrderResponse addOrderItem(@Valid @PathVariable(name = "id") Long id,
                                      @RequestBody OrderRequest orderRequest) {
        return orderService.addOrderItem(id, orderRequest);
    }

    @PutMapping("/removeorderitem/{id}")
    public OrderResponse removeOrderItem(@PathVariable(name = "id") Long id,
                                         @RequestBody OrderResponse orderResponse) {
        return orderService.removeOrderItem(id, orderResponse);
    }


    @PutMapping("/updateStatusPaid/{id}")
    public OrderResponse updateStatusPaid(@Valid @PathVariable(name = "id") Long id) {
        return orderService.updateStatusPaid(id);
    }


    @PutMapping("/updateStatusCancel/{id}")
    public OrderResponse updateStatusCancel(@PathVariable(name = "id") Long id) {
        return orderService.updateStatusCancel(id);
    }
}
