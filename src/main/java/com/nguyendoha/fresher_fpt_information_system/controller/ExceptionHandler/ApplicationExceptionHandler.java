package com.nguyendoha.fresher_fpt_information_system.controller.ExceptionHandler;

import com.nguyendoha.fresher_fpt_information_system.constant.Constant;
import com.nguyendoha.fresher_fpt_information_system.exception.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = CustomerNotFound.class)
    public ResponseEntity<?> CustomerNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.CUSTOMER_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = OrderItemNotFound.class)
    public ResponseEntity<?> OrderItemNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.ORDER_ITEM_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = ProductError.class)
    public ResponseEntity<?> ProductError(Exception e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(Message.builder()
                        .code(Constant.PRODUCT_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = ProductNotFound.class)
    public ResponseEntity<?> ProductNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.PRODUCT_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = OrderNotFound.class)
    public ResponseEntity<?> OrderNotFound(Exception e) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(Message.builder()
                        .code(Constant.ORDER_ITEM_NOT_FOUND)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = OrderError.class)
    public ResponseEntity<?> OrderError(Exception e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(Message.builder()
                        .code(Constant.ORDER_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = OrderItemError.class)
    public ResponseEntity<?> OrderItemError(Exception e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(Message.builder()
                        .code(Constant.ORDER_ITEM_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> Exception(Exception e) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Message.builder()
                        .code(Constant.INTERNAL_SERVER_ERROR)
                        .time(LocalDateTime.now())
                        .message(e.getMessage())
                        .build());
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ResponseEntity<?> ConstraintViolationException(ConstraintViolationException e) {
        List<String> errorMessages = e.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage).toList();
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(Message.builder()
                        .code(Constant.VALIDATION_ERROR)
                        .time(LocalDateTime.now())
                        .message(errorMessages.toString())
                        .build());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(Message.builder()
                        .code(Constant.VALIDATION_ERROR)
                        .time(LocalDateTime.now())
                        .message(Objects.requireNonNull(ex.getBindingResult().getFieldError()).getDefaultMessage())
                        .build());
    }
}
