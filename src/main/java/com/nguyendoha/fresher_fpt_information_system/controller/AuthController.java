package com.nguyendoha.fresher_fpt_information_system.controller;


import com.nguyendoha.fresher_fpt_information_system.dto.request.LoginRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.request.SignupRequest;
import com.nguyendoha.fresher_fpt_information_system.service.AuthenticationAndCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthenticationAndCustomerService authenticationService;

    @Autowired
    public AuthController(AuthenticationAndCustomerService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return authenticationService.authenticateUser(loginRequest);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return authenticationService.registerUser(signUpRequest);
    }

    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser() {
        return authenticationService.logoutUser();
    }
}
