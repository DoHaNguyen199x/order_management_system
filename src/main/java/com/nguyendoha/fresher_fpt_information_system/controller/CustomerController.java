package com.nguyendoha.fresher_fpt_information_system.controller;

import com.nguyendoha.fresher_fpt_information_system.dto.request.SignupRequest;
import com.nguyendoha.fresher_fpt_information_system.dto.response.CustomerResponse;
import com.nguyendoha.fresher_fpt_information_system.service.AuthenticationAndCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@RestController
@RequestMapping("/api/customer")
public class CustomerController {
    private final AuthenticationAndCustomerService andCustomerService;

    @Autowired
    public CustomerController(AuthenticationAndCustomerService andCustomerService) {
        this.andCustomerService = andCustomerService;
    }

    @GetMapping("/{n}/{s}")
    public Page<CustomerResponse> findAllUser(@PathVariable(name = "n") Integer n
            , @PathVariable(name = "s") Integer s) {
        return andCustomerService.findAllUser(PageRequest.of(n, s));
    }

    @GetMapping("/{id}")
    public CustomerResponse findById(@PathVariable(name = "id") Long id) {
        return andCustomerService.findById(id);
    }

    @PutMapping("/{id}")
    public CustomerResponse updateUser(@PathVariable(name = "id") Long id,
                                       @RequestBody SignupRequest signupRequest) {
        return andCustomerService.updateUser(id, signupRequest);
    }

    @PutMapping("/removeroleuser/{id}")
    public CustomerResponse removeRoleUser(@PathVariable(name = "id") Long id,
                                           @RequestBody SignupRequest signupRequest) {
        return andCustomerService.removeRoleUser(id, signupRequest);
    }

    @PostMapping("/addroleuser/{id}")
    public CustomerResponse addRoleUser(@PathVariable(name = "id") Long id,
                                        @RequestBody SignupRequest signupRequest) {
        return andCustomerService.addRoleUser(id, signupRequest);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable(name = "id") Long id) {
        return andCustomerService.deleteUser(id);
    }
}
