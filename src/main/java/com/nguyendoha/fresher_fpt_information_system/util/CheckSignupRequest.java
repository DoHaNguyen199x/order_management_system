package com.nguyendoha.fresher_fpt_information_system.util;

import com.nguyendoha.fresher_fpt_information_system.dto.request.SignupRequest;
import com.nguyendoha.fresher_fpt_information_system.exception.EmailAlreadyUsedException;
import com.nguyendoha.fresher_fpt_information_system.exception.PasswordError;
import com.nguyendoha.fresher_fpt_information_system.exception.PhoneError;
import com.nguyendoha.fresher_fpt_information_system.exception.UserNameAlreadyUsedException;
import com.nguyendoha.fresher_fpt_information_system.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Component
public final class CheckSignupRequest {
    private final CustomerRepository customerRepository;

    @Autowired
    public CheckSignupRequest(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public void check(SignupRequest signupRequest) {

        if (signupRequest.getUserName() != null) {
            if (customerRepository.existsByUserName(signupRequest.getUserName())) {
                throw new UserNameAlreadyUsedException("Username is already taken!");
            }
        }
        if (signupRequest.getEmail() != null) {
            if (customerRepository.existsByEmail(signupRequest.getEmail())) {
                throw new EmailAlreadyUsedException("Email is already in use !!!");
            }
        }
        if (signupRequest.getPassword() != null) {
            if (!signupRequest.getPassword().equals(signupRequest.getPasswordAgain())) {
                throw new PasswordError("Passwords are not the same !!!");
            }
        }
        if (signupRequest.getPhone() != null) {
            boolean checkPhone = Pattern.compile("^\\d{10}$").matcher(signupRequest.getPhone()).matches();
            if (!checkPhone) {
                throw new PhoneError("phone syntax error , please try again !!!");
            }
        }

    }
}
