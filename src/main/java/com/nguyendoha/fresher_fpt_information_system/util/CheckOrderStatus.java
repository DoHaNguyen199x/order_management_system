package com.nguyendoha.fresher_fpt_information_system.util;

import com.nguyendoha.fresher_fpt_information_system.entity.Order;
import com.nguyendoha.fresher_fpt_information_system.entity.enums.OrderStatus;
import com.nguyendoha.fresher_fpt_information_system.exception.OrderError;
import org.springframework.stereotype.Component;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Component
public final class CheckOrderStatus {
    public void check(Order order) {
        if (!order.getStatus().equals(OrderStatus.CREATED)) {
            throw new OrderError("order status else created !!!");
        }
    }
}
