package com.nguyendoha.fresher_fpt_information_system.util;

import com.nguyendoha.fresher_fpt_information_system.entity.Order;
import com.nguyendoha.fresher_fpt_information_system.entity.OrderItem;
import com.nguyendoha.fresher_fpt_information_system.exception.OrderError;
import org.springframework.stereotype.Component;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Component
public final class CheckOrderItemByOrder {
    public void check(Order order, OrderItem orderItem) {
        if (!orderItem.getOrder().getId().equals(order.getId())) {
            throw new OrderError("order item not by order id !!!");
        }
    }
}
