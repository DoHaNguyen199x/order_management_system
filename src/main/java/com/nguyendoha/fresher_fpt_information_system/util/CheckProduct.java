package com.nguyendoha.fresher_fpt_information_system.util;

import com.nguyendoha.fresher_fpt_information_system.dto.request.OrderItemRequest;
import com.nguyendoha.fresher_fpt_information_system.entity.Product;
import com.nguyendoha.fresher_fpt_information_system.exception.ProductError;
import org.springframework.stereotype.Component;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Component
public final class CheckProduct {
    public void check(Product product, OrderItemRequest orderItemRequest) {
        if (product.getAvailable() < orderItemRequest.getQuantity()) {
            throw new ProductError("Product quantity is not enough !!!" +
                    "\n Product available = " + product.getAvailable());
        }
    }
}
