package com.nguyendoha.fresher_fpt_information_system;

import com.nguyendoha.fresher_fpt_information_system.entity.Role;
import com.nguyendoha.fresher_fpt_information_system.entity.enums.ERole;
import com.nguyendoha.fresher_fpt_information_system.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@SpringBootApplication
public class FresherFptInformationSystemApplication {
    private static RoleRepository roleRepository;

    @Autowired
    public FresherFptInformationSystemApplication(RoleRepository roleRepository) {
        FresherFptInformationSystemApplication.roleRepository = roleRepository;
    }

    // run class FresherFptInformationSystem and role auto add database
    public static void main(String[] args) {
        SpringApplication.run(FresherFptInformationSystemApplication.class, args);
        if (roleRepository.findByName(ERole.ROLE_ADMIN).isEmpty()
                || roleRepository.findByName(ERole.ROLE_USER).isEmpty()) {
            List<Role> roles = new ArrayList<>();
            roles.add(Role.builder().name(ERole.ROLE_ADMIN).build());
            roles.add(Role.builder().name(ERole.ROLE_USER).build());
            roleRepository.saveAll(roles);
        }
    }


}
