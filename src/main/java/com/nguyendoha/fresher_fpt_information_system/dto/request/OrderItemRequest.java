package com.nguyendoha.fresher_fpt_information_system.dto.request;

import lombok.Data;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
public class OrderItemRequest {
    private Long id;
    private Long product;
    private Long quantity;
    private Double amount;
}
