package com.nguyendoha.fresher_fpt_information_system.dto.request;


import lombok.Data;

/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
import java.time.LocalDateTime;
import java.util.List;

@Data
public class OrderRequest {
    private Long id;
    private LocalDateTime orderDateTime;
    private Long customer;
    private List<OrderItemRequest> orderItems;
    private Double totalAmount;
    private String status;
}
