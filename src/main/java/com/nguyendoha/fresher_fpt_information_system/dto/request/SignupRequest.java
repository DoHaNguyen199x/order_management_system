package com.nguyendoha.fresher_fpt_information_system.dto.request;



import lombok.Data;
import javax.validation.constraints.Size;
import java.util.List;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
public class SignupRequest {
    private String name;
    @Size(min = 5, max = 30, message = "user name > 5 and < 30 character !!!")
    private String userName;
    @Size(min = 6, max = 30, message = "password > 6 and < 30 character !!!")
    private String password;
    private String passwordAgain;
    private String email;
    private String phone;
    private String address;
    private List<String> roles;
}
