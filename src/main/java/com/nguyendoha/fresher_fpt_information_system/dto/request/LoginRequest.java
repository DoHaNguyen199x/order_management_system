package com.nguyendoha.fresher_fpt_information_system.dto.request;

import lombok.Data;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Data
public class LoginRequest {
    private String userName;
    private String password;
}
