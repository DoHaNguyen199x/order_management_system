package com.nguyendoha.fresher_fpt_information_system.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.nguyendoha.fresher_fpt_information_system.entity.Order;
import com.nguyendoha.fresher_fpt_information_system.entity.enums.OrderStatus;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Builder
@Data
public class OrderResponse {
    private Long id;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime orderDateTime;
    private CustomerResponse customer;
    private List<OrderItemResponse> orderItems;
    private Double totalAmount;
    private OrderStatus status;

    public static OrderResponse fromEntity(Order order) {
        return OrderResponse.builder()
                .id(order.getId())
                .orderDateTime(order.getOrderDateTime())
                .customer(CustomerResponse.fromEntity(order.getCustomer()))
                .orderItems(order.getOrderItems().stream().map(OrderItemResponse::fromEntity).toList())
                .totalAmount(order.getTotalAmount())
                .status(order.getStatus())
                .build();
    }
}
