package com.nguyendoha.fresher_fpt_information_system.dto.response;

import com.nguyendoha.fresher_fpt_information_system.entity.Customer;
import com.nguyendoha.fresher_fpt_information_system.entity.Role;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Builder
@Data
public class CustomerResponse {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String address;
    private List<String> roles;

    public static CustomerResponse fromEntity(Customer customer) {
        List<String> roles = new ArrayList<>();
        customer.getRoles().forEach(role -> {
            roles.add(String.valueOf(role));
        });
        return CustomerResponse.builder()
                .id(customer.getId())
                .name(customer.getName())
                .email(customer.getEmail())
                .phone(customer.getPhone())
                .address(customer.getAddress())
                .roles(roles)
                .build();
    }
}
