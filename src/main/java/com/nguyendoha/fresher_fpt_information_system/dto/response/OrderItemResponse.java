package com.nguyendoha.fresher_fpt_information_system.dto.response;

import com.nguyendoha.fresher_fpt_information_system.entity.OrderItem;
import com.nguyendoha.fresher_fpt_information_system.entity.Product;
import lombok.Builder;
import lombok.Data;
/**
 * @author DoHaNguyen
 * @facebook https://www.facebook.com/doha.nguyendoha
 */
@Builder
@Data
public class OrderItemResponse {
    private Long id;
    private Product product;
    private Long quantity;
    private Double amount;

    public static OrderItemResponse fromEntity(OrderItem orderItem) {
        return OrderItemResponse.builder()
                .id(orderItem.getId())
                .product(orderItem.getProduct())
                .quantity(orderItem.getQuantity())
                .amount(orderItem.getAmount())
                .build();
    }
}
