#Order Management System Create By DoHaNguyen.
----
> - Project Completion :heavy_check_mark: :heavy_check_mark: :heavy_check_mark: .
---
@ Database :
>> - create database nguyendoha_final.
> - Run Spring Boot Application Role auto insert in database.
> 
> ![img.png](img.png)

----
@Api : 

- http://localhost:8080/api/auth/** permit All 
- http://localhost:8080/api/customer/** has Role : user ,admin.
- http://localhost:8080/api/order/** has Role : user ,admin.
- http://localhost:8080/api/product/** has role : admin.